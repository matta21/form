    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */
    package com.mycompany.apiresteva.entity;

    import java.io.Serializable;
    import javax.persistence.Basic;
    import javax.persistence.Column;
    import javax.persistence.Entity;
    import javax.persistence.Id;
    import javax.persistence.NamedQueries;
    import javax.persistence.NamedQuery;
    import javax.persistence.Table;
    import javax.validation.constraints.NotNull;
    import javax.validation.constraints.Size;
    import javax.xml.bind.annotation.XmlRootElement;

    /**
     *
     * @author stark
     */
    @Entity
    @Table(name = "form")
    @XmlRootElement
    @NamedQueries({
        @NamedQuery(name = "Form.findAll", query = "SELECT f FROM Form f"),
        @NamedQuery(name = "Form.findByRut", query = "SELECT f FROM Form f WHERE f.rut = :rut"),
        @NamedQuery(name = "Form.findByNombre", query = "SELECT f FROM Form f WHERE f.nombre = :nombre"),
        @NamedQuery(name = "Form.findByApellido", query = "SELECT f FROM Form f WHERE f.apellido = :apellido"),
        @NamedQuery(name = "Form.findByEdad", query = "SELECT f FROM Form f WHERE f.edad = :edad")})
    public class Form implements Serializable {

        private static final long serialVersionUID = 1L;
        @Id
        @Basic(optional = false)
        @NotNull
        @Size(min = 1, max = 50)
        @Column(name = "Rut")
        private String rut;
        @Size(max = 50)
        @Column(name = "Nombre")
        private String nombre;
        @Size(max = 50)
        @Column(name = "Apellido")
        private String apellido;
        @Size(max = 50)
        @Column(name = "Edad")
        private String edad;

        public Form() {
        }

        public Form(String rut) {
            this.rut = rut;
        }

        public String getRut() {
            return rut;
        }

        public void setRut(String rut) {
            this.rut = rut;
        }

        public String getNombre() {
            return nombre;
        }

        public void setNombre(String nombre) {
            this.nombre = nombre;
        }

        public String getApellido() {
            return apellido;
        }

        public void setApellido(String apellido) {
            this.apellido = apellido;
        }

        public String getEdad() {
            return edad;
        }

        public void setEdad(String edad) {
            this.edad = edad;
        }

        @Override
        public int hashCode() {
            int hash = 0;
            hash += (rut != null ? rut.hashCode() : 0);
            return hash;
        }

        @Override
        public boolean equals(Object object) {
            // TODO: Warning - this method won't work in the case the id fields are not set
            if (!(object instanceof Form)) {
                return false;
            }
            Form other = (Form) object;
            if ((this.rut == null && other.rut != null) || (this.rut != null && !this.rut.equals(other.rut))) {
                return false;
            }
            return true;
        }

        @Override
        public String toString() {
            return "root.mycompany.apiresteva.entity.Form[ rut=" + rut + " ]";
        }

      
        

    }
