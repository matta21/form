/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.apiresteva;

import com.mycompany.apiresteva.dao.FormJpaController;
import com.mycompany.apiresteva.dao.exceptions.NonexistentEntityException;
import com.mycompany.apiresteva.entity.Form;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Carlos Matta
 */

@Path("Formulario")
public class Formulario {
    
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response Listarformu() {
      
       
        FormJpaController dao=new FormJpaController();
        List<Form> lista= dao.findFormEntities();
        
        return Response.ok(200).entity(lista).build();
        
    }
    
    
    @POST
     @Produces(MediaType.APPLICATION_JSON)
    public Response add(Form Formulario){
        
        try {
            FormJpaController dao=new FormJpaController();
            dao.create(Formulario);
        } catch (Exception ex) {
            Logger.getLogger(Formulario.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.ok(200).entity(Formulario).build();
        
    }
    
    
     @DELETE
     @Path("/{iddelete}")
     @Produces(MediaType.APPLICATION_JSON)
        public Response delete(@PathParam("iddelete") String iddelete ){
        
        try {
            FormJpaController dao=new FormJpaController();
            dao.destroy(iddelete);
            
            
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(Formulario.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return Response.ok("cliente eliminado").build();
        
    }
   
    @PUT    
    public Response update(Form Formulario){
        
        try {
            FormJpaController dao=new FormJpaController();
            dao.edit(Formulario);
        } catch (Exception ex) {
            Logger.getLogger(Formulario.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
         return Response.ok(200).entity(Formulario).build();
    }
   
}
 

